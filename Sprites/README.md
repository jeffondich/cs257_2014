This repository contains an IntelliJ JavaFX project for an all-class assignment for Carleton College's CS 257, Fall 2014.

Please be careful when pushing, since the whole class will be using this project. Remember, when you're ready to push:

     git commit
     git pull
     git commit [only if the pull required a merge]
     [if the pull brought new stuff, make sure the result compiles and runs]
     git push

Ideally, you do all of this fairly quickly so nobody else pushes between your pull and your push. It should work even if that does happen, in most cases.